const express = require('express');
const port = 5050;
const app = express()


let item = [{
    name: "Mjolnir",
    price: 50000,
    isActive: true
   },
   {
    name: "Vibranium Shield",
    price:70000,
    isActive: true
   }
];

app.get('/', (req,res) =>{
    res.send("Welcome to my Shop!")
})

//Create a Get route that will access home

app.get('/home', (req,res) =>{
    res.send("Welcome to my Shop!! this my homepage!")
})

//Process a Get request at the home rouste using post man!
//DONE

//Create GET rout that will access the /items route wull retrieve all the users in the mockup database.

app.get('/items',(req,res)=>{
    res.send(item)
})

//process a GET request at the /items route using postman
//done

//create a delete route that will access the /delete-item route to remove a item from mockup database

app.delete('/delete-item', (req,res)=>{
    let deleteItem = item.pop()
    res.send(deleteItem);
})

app.listen(port, () => console.log(`The Server is running at ${port}`));